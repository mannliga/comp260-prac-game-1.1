﻿
using UnityEngine;
using System.Collections;


public class BeeMove : MonoBehaviour {

	//public parameters with default values
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	//private state
	private float speed;
	private float turnSpeed;

	// Use this for initialization
	void Start () {
		//find a player object to be the target by type
		//note: this is not standard unity syntax
		PlayerMove player = FindObjectOfType<PlayerMove> ();
		target = player.transform;

		PlayerMove2 player2 = FindObjectOfType<PlayerMove2> ();
		target2 = player2.transform;

		//bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate (angle);

		//set speed and turnSpeed randomly
		speed = Mathf.Lerp(1, 5, Random.value);
		turnSpeed = Mathf.Lerp (1, 360, Random.value);
	}
	public Transform target, target2;
	public Vector2 heading = Vector3.right; 



	void Update() {



		// get the vector from the bee to the target 
		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target2.position - transform.position;

		if(direction.magnitude >= direction2.magnitude) {

			// calculate how much to turn per frame
			float angle = turnSpeed * Time.deltaTime;

			// turn left or right
			if (direction2.IsOnLeft(heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate(angle);
			}
			else {
				// target on right, rotate clockwise
				heading = heading.Rotate(-angle);
			}

			transform.Translate(heading * speed * Time.deltaTime);
		} else {

			// calculate how much to turn per frame
			float angle = turnSpeed * Time.deltaTime;

			// turn left or right
			if (direction.IsOnLeft(heading)) {
				// target on left, rotate anticlockwise
				heading = heading.Rotate(angle);
			}
			else {
				// target on right, rotate clockwise
				heading = heading.Rotate(-angle);
			}

			transform.Translate(heading * speed * Time.deltaTime);


		}

	}

	public ParticleSystem explosionPrefab;

	void  OnDestroy () {
		//create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate (explosionPrefab);
		explosion.transform.position = transform.position;
		//destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}


/*	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Vector2 direction2 = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
*/

}


